﻿Imports System.Console
Imports System.IO
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Configuration
Imports System.Net
Imports System.Web
Imports System.Xml


Module Module1
  Dim H As Byte = 99
  Dim DRN As Byte = 0
  Dim DT_DRN As Date = Nothing
  Private WB As New WebAccess

  Sub Main()
    CursorVisible = False
    Echo("┌─────────────────────┐")
    Echo("│ GRD Load " & ("v" & My.Application.Info.Version.ToString(3)).PadLeft(10) & " │")
    Echo("└─────────────────────┘") : Echo()

    While True
      'Try
      'Dim CSV_URL As String = "http://localhost:88/OT/GRD.csv"
      'Dim RS As Object = WB.Load(CSV_URL)
      'GetEvents(RS)

      If H <> Math.Floor(Now.Second / 30) Then
        H = Math.Floor(Now.Second / 30)
        Echo("Checking if there's any new file..")
        MoveFileToProcessing()
        ProcessFiles()
      End If
      Thread.Sleep(5000)
      'Catch : Log("MAIN" & vbTab & Err.Description) : End Try
    End While
  End Sub

  Sub MoveFileToProcessing()
    For Each fileFound As String In Directory.GetFiles(My.Settings.NEW_FOLDER)
      Echo(fileFound)
      Dim destFile = System.IO.Path.GetFileName(fileFound)
      File.Move(fileFound, My.Settings.PROCESSING_FOLDER & destFile)
    Next
  End Sub

  Sub ProcessFiles()
    For Each fileFound As String In Directory.GetFiles(My.Settings.PROCESSING_FOLDER)
      Dim destFile = System.IO.Path.GetFileName(fileFound)
      Dim GE_STATUS As String = ""
      Using SR As StreamReader = New StreamReader(fileFound)
        Dim I As Integer = 0, J As Integer = 0
        Dim RS As String = SR.ReadToEnd()
        Echo("Processing " & destFile & "...")
        GE_STATUS = GetEvents(RS)
      End Using
      If GE_STATUS = "OK" Then
        File.Move(fileFound, My.Settings.DONE_FOLDER & destFile)
      Else
        File.Move(fileFound, My.Settings.CHECK_FOLDER & destFile)
      End If
    Next
  End Sub
  Function GetEvents(ByVal RS As Object) As String
    'Try
    Dim I As Integer = 0, J As Integer = 1
    Dim RWS As Object = Split(RS, vbCrLf)
    Dim NW As Boolean = False, TN, CT, DT, RN, MD As Object, MDS : MD = 0 : MDS = "" : Dim TC As Integer = 0
    Dim JT As String = "NAR"
    Dim JC As Byte = 0
    Dim ST As String = ""

    For Each R As Object In RWS
      If I > 0 AndAlso I < UBound(RWS) Then
        Dim C As Object = Split(R, """,""")
        Select Case rAP(C(2))
          Case "AUS", "NZ", "HK", "FR", "USA", "CAN", "JPN", "QA", "BHR", "PR" : CT = Left(rAP(C(2)), 2)
          Case "GB" : CT = "UK"
          Case "IRE" : CT = "IE"
          Case "SIN" : CT = "SG"
          Case "GER" : CT = "DE"
          Case "UAE" : CT = "AE"
          Case "SAF" : CT = "ZA"
          Case "CHN" : CT = "CN"
          Case "KOR" : CT = "KR"
          Case "KSA" : CT = "SA"
          Case Else : CT = "ZZ"
        End Select

        If My.Settings.CT_LIST.Contains(CT) Then
          '0:FieldMeetingID ; 1:MeetingDate ; 2:CountryAbbrev ; 3:TrackName ; 4:RailDesc ; 5:FieldRaceID ; 6:RaceNo ; 
          '7:RaceStartTime ; 8:RaceStartTimeUTC ; 9:ToteIndicator ; 10:RaceName ; 11:Distance ; 12:ClassRestriction ; 13:AgeRestriction ; 
          '14:SexRestriction ; 15:BreedRestriction ; 16:HandicapType ; 17:RacePrizemoney ; 18:NRaceName ; 19:GoingName ; 20:FieldID ; 
          '21:Saddlecloth ; 22:CouplingGroup ; 23:HorseName ; 24:JockeyName ; 25:TrainerName ; 26:StartingBarrier ; 27:HandicapWeight ; 
          '28:RunnerStatus ; 29:ReportedCareerStarts ; 30:MeetingCreated ; 31:MeetingLastUpdated ; 32:RaceCreated ; 33:RaceLastUpdated ; 34:RunnerCreated ; 
          '35:RunnerLastUpdated ; 36:SaddleClothSort ; 37:TimeZoneName ; 38:TimeZoneDesc ; 39:JumpsRace ; 40:StateAbbrev ; 41:RacingType ; 
          '42:RacingSubType ; 43:StartType
          If TN <> rAP(C(3)) OrElse DT <> CDate(rAP(C(1))).ToString("yyyy-MM-dd") Then
            If DT <> CDate(rAP(C(1))).ToString("yyyy-MM-dd") Then TC = 1 Else TC += 1
            TN = rAP(C(3))

            DT = CDate(rAP(C(1))).ToString("yyyy-MM-dd")
            Echo(DT & vbTab & TN.ToUpper)
            NW = True
            If CT = "JP" Then
              Select Case TN
                Case "FUNABASHI" : JC = 19
                Case "KANAZAWA" : JC = 22
                Case "KASAMATSU" : JC = 23
                Case "KAWASAKI" : JC = 21
                Case "KOUCHI" : JC = 31
                Case "MIZUSAWA" : JC = 11
                Case "MONBETSU" : JC = 36
                Case "MORIOKA" : JC = 10
                Case "NAGOYA" : JC = 24
                Case "OBIHIRO" : JC = 3
                Case "OOI" : JC = 20
                Case "SAGA" : JC = 32
                Case "SONODA" : JC = 27
                Case "URAWA" : JC = 18
                Case Else : JC = 0
              End Select

              Select Case rAP(C(9))
                Case "J" : JT = "JRA"
                Case "N" : JT = "NAR"
                Case Else : JT = "NAR"
              End Select
              If getResult("SELECT COUNT(*) FROM MEETING(nolock) WHERE MEETING_DATE = '" & DT & "' AND COUNTRY = 'JP' AND TYPE = 'R' AND TRK_INFO = '" & JT & "' AND VENUE = '" & TN & "'") = 0 Then
                ST &= vbLf & "INSERT MEETING(MEETING_DATE, COUNTRY, TYPE, TRK_INFO, VENUE, TRK_CODE, JP_CODE, GRD_MEETING_ID) VALUES ('" & DT & "', 'JP', 'R', '" & JT & "', " & cQS(TN) & ", " & TC & ", " & JC & ", " & cQS(rAP(C(0))) & ") SELECT @@IDENTITY"
                ''Buat Insert Meeting Baru (JP)
                MD = getResult("INSERT MEETING(MEETING_DATE, COUNTRY, TYPE, TRK_INFO, VENUE, TRK_CODE, JP_CODE, GRD_MEETING_ID) VALUES ('" & DT & "', 'JP', 'R', '" & JT & "', " & cQS(TN) & ", " & TC & ", " & JC & ", " & cQS(rAP(C(0))) & ") SELECT @@IDENTITY")
                MDS &= "," & MD
              Else
                ST &= vbLf & "SELECT MEETING_ID FROM MEETING(nolock) WHERE MEETING_DATE = '" & DT & "' AND COUNTRY = 'JP' AND TYPE = 'R' AND TRK_INFO = '" & JT & "' AND VENUE = '" & TN & "'"
                ''Buat Ambil Meeting ID yang uda ada (JP)
                MD = getResult("SELECT MEETING_ID FROM MEETING(nolock) WHERE MEETING_DATE = '" & DT & "' AND COUNTRY = 'JP' AND TYPE = 'R' AND TRK_INFO = '" & JT & "' AND VENUE = '" & TN & "'")
              End If
            Else
              If getResult("SELECT COUNT(*) FROM MEETING(nolock) WHERE MEETING_DATE = '" & DT & "' AND COUNTRY = '" & CT & "' AND TYPE = 'R' AND VENUE = '" & TN & "'") = 0 Then
                ST &= vbLf & "INSERT MEETING(MEETING_DATE, COUNTRY, TYPE, VENUE, TRK_CODE, GRD_MEETING_ID) VALUES ('" & DT & "', '" & CT & "', 'R', " & cQS(TN) & ", " & TC & ", " & cQS(rAP(C(0))) & ") SELECT @@IDENTITY"
                ''Buat Insert Meeting Baru (everything except JP)
                MD = getResult("INSERT MEETING(MEETING_DATE, COUNTRY, TYPE, VENUE, TRK_CODE, GRD_MEETING_ID) VALUES ('" & DT & "', '" & CT & "', 'R', " & cQS(TN) & ", " & TC & ", " & cQS(rAP(C(0))) & ") SELECT @@IDENTITY")
                MDS &= "," & MD
              Else
                ST &= vbLf & "SELECT MEETING_ID FROM MEETING(nolock) WHERE MEETING_DATE = '" & DT & "' AND COUNTRY = '" & CT & "' AND TYPE = 'R' AND VENUE = '" & TN & "'"
                ''Buat Ambil Meeting ID yg uda ada (everything except JP)
                MD = getResult("SELECT MEETING_ID FROM MEETING(nolock) WHERE MEETING_DATE = '" & DT & "' AND COUNTRY = '" & CT & "' AND TYPE = 'R' AND VENUE = '" & TN & "'")
              End If
            End If
          Else : NW = False
          End If

          If NW Or (RN <> rAP(C(6)) And MD > 0) Then
            RN = rAP(C(6))
            If getResult("SELECT COUNT(*) FROM EVENT(nolock) WHERE MEETING_ID = " & MD & " AND EVENT_NO = " & RN) = 0 Then
              ST &= vbLf & vbTab & "INSERT EVENT(MEETING_ID, EVENT_NO, START_TIME, NAME, JUMP_TYPE, STATUS, VID_STATUS, DISTANCE, GRD_EVENT_ID) VALUES (" & MD & ", " & RN & ", '" & CDate(rAP(C(8))).ToString("yyyy-MM-dd HH:mm:ss") & "', " & cQS(rAP(C(10))) & ", '" & If(cQS(rAP(C(10))) = "NULL", "F", If(rAP(C(10)).Contains("CHASE"), "C", If(rAP(C(10)).Contains("HURDLE"), "H", "F"))) & "', 'DONE', 'RAW', " & rAP(C(11)) & ", " & cQS(rAP(C(5))) & ")"
              ''Buat Insert Event Baru
              execSQL("INSERT EVENT(MEETING_ID, EVENT_NO, START_TIME, NAME, JUMP_TYPE, STATUS, VID_STATUS, DISTANCE, GRD_EVENT_ID) VALUES (" & MD & ", " & RN & ", '" & CDate(rAP(C(8))).ToString("yyyy-MM-dd HH:mm:ss") & "', " & cQS(rAP(C(10))) & ", '" & If(cQS(rAP(C(10))) = "NULL", "F", If(rAP(C(10)).Contains("CHASE"), "C", If(rAP(C(10)).Contains("HURDLE"), "H", "F"))) & "', 'DONE', 'RAW', " & rAP(C(11)) & ", " & cQS(rAP(C(5))) & ")")
              J = 1
            Else
              If "KR|JP".Contains(CT) Then
                J = getResult("SELECT MAX(RUNNER_NO) FROM RUNNER(nolock) WHERE MEETING_ID = " & MD & " AND EVENT_NO = " & RN) + 1
              Else
                J = getResult("SELECT MAX(RUNNER_NO) FROM RUNNER(nolock) WHERE MEETING_ID = " & MD & " AND EVENT_NO = " & RN)
                If Not IsDBNull(J) Then J = J + 1
              End If
            End If
          End If

          If MD > 0 Then
            '0:FieldMeetingID ; 1:MeetingDate ; 2:CountryAbbrev ; 3:TrackName ; 4:RailDesc ; 5:FieldRaceID ; 6:RaceNo ; 
            '7:RaceStartTime ; 8:RaceStartTimeUTC ; 9:ToteIndicator ; 10:RaceName ; 11:Distance ; 12:ClassRestriction ; 13:AgeRestriction ; 
            '14:SexRestriction ; 15:BreedRestriction ; 16:HandicapType ; 17:RacePrizemoney ; 18:NRaceName ; 19:GoingName ; 20:FieldID ; 
            '21:Saddlecloth ; 22:CouplingGroup ; 23:HorseName ; 24:JockeyName ; 25:TrainerName ; 26:StartingBarrier ; 27:HandicapWeight ; 
            '28:RunnerStatus ; 29:ReportedCareerStarts ; 30:MeetingCreated ; 31:MeetingLastUpdated ; 32:RaceCreated ; 33:RaceLastUpdated ; 34:RunnerCreated ; 
            '35:RunnerLastUpdated ; 36:SaddleClothSort ; 37:TimeZoneName ; 38:TimeZoneDesc ; 39:JumpsRace ; 40:StateAbbrev ; 41:RacingType ; 
            '42:RacingSubType ; 43:StartType
            If getResult("SELECT COUNT(*) FROM RUNNER(nolock) WHERE MEETING_ID = " & MD & " AND EVENT_NO = " & RN & " AND NAME = " & cQS(rAP(C(23)))) = 0 Then
              ST &= vbLf & vbTab & vbTab & "INSERT RUNNER(MEETING_ID, EVENT_NO, RUNNER_NO, NAME, JOCKEY, TRAINER, BARRIER, HANDICAP, SCR, GRD_RUNNER_ID) VALUES (" & MD & ", " & RN & ", '" & If(rAP(C(21)) = "", J, rAP(C(21))) & "', " & cQS(rAP(C(23))) & ", " & cQS(rAP(C(24))) & ", " & cQS(rAP(C(25))) & ", " & cQS(rAP(C(26))) & ", " & cQS(rAP(C(27))) & ", " & If(rAP(C(28)) = "S", 1, 0) & ", " & cQS(rAP(C(20))) & ")"
              ''Buat Insert Runner Baru
              execSQL("INSERT RUNNER(MEETING_ID, EVENT_NO, RUNNER_NO, NAME, JOCKEY, TRAINER, BARRIER, HANDICAP, SCR, GRD_RUNNER_ID) VALUES (" & MD & ", " & RN & ", '" & If(rAP(C(21)) = "", J, rAP(C(21))) & "', " & cQS(rAP(C(23))) & ", " & cQS(rAP(C(24))) & ", " & cQS(rAP(C(25))) & ", " & cQS(rAP(C(26))) & ", " & cQS(rAP(C(27))) & ", " & If(rAP(C(28)) = "S", 1, 0) & ", " & cQS(rAP(C(20))) & ")")
            End If
            J += 1
          End If
        End If
      End If
      I += 1
    Next
    If MDS <> "" Then : MDS = Mid(MDS, 2)
      ST &= vbLf & vbTab & vbTab & vbTab & "UPDATE MEETING SET EVENTS = (SELECT COUNT(*) FROM EVENT WHERE EVENT.MEETING_ID = MEETING.MEETING_ID) WHERE MEETING_ID IN(" & MDS & ")"
      ST &= vbLf & vbTab & vbTab & vbTab & "UPDATE EVENT SET STARTERS = (SELECT COUNT(RUNNER_NO) FROM RUNNER WHERE EVENT.MEETING_ID = RUNNER.MEETING_ID AND EVENT.EVENT_NO = RUNNER.EVENT_NO) WHERE MEETING_ID IN(" & MDS & ")"
      ''Update Starters and Number of Events in EVENT table and MEETING table respectively  
      execSQL("UPDATE MEETING SET EVENTS = (SELECT COUNT(*) FROM EVENT WHERE EVENT.MEETING_ID = MEETING.MEETING_ID) WHERE MEETING_ID IN(" & MDS & ")")
      execSQL("UPDATE EVENT SET STARTERS = (SELECT COUNT(RUNNER_NO) FROM RUNNER WHERE EVENT.MEETING_ID = RUNNER.MEETING_ID AND EVENT.EVENT_NO = RUNNER.EVENT_NO) WHERE MEETING_ID IN(" & MDS & ")")
    End If

    If My.Settings.TEST_FLAG Then Log(ST)
    Return "OK"

    'Catch : Return "GetEvents - CHECK" : End Try
  End Function

  Sub TestTimeZone()
    Dim TZInfo As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time")
    Dim DT As DateTime = TimeZoneInfo.ConvertTime(DateTime.Now, TZInfo)
    Echo(DT.ToString("yyyy-MM-dd HH:mm:ss"))
  End Sub

  Function ToAUSTime(ByVal D As DateTime) As String
    Dim TZInfo As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time")
    Dim DT As DateTime = TimeZoneInfo.ConvertTime(D, TZInfo)
    Return DT.ToString("yyyy-MM-dd HH:mm:ss")
  End Function


#Region "Useful Functions"
  Function xAT(N As XmlNode, A As String, Optional D As String = "") As String
    Try : Return N.Attributes(A).InnerText : Catch : Return D : End Try
  End Function

  Function rAP(X As Object) As String
    Return Replace(X, """", "")
  End Function

  Function cQS(X As Object, Optional CN As Boolean = True) As String
    If IsDBNull(X) OrElse IsNothing(X) OrElse Len(X) = 0 Then Return "NULL"
    If CN AndAlso IsNumeric(X) Then Return X Else Return "'" & Replace(X, "'", "''") & "'"
  End Function


  Sub Echo(Optional X As String = "", Optional R As Boolean = False)
    Write(If(X <> "", Now.ToString("HH:mm:ss ") & X, "") & If(R, "", vbCrLf))
  End Sub

  Sub Ink(ByVal ParamArray X() As Object)
    Dim Y As String = ""
    If Not IsNothing(X) Then
      Y = Now.ToString("HH:mm:ss ")
      For I = 0 To UBound(X) : Y &= X(I) & " " : Next
    End If : Y &= vbCrLf
    Write(Y)
  End Sub


  Sub Paint(X As Byte, Y As Byte, S As String)
    SyncLock GetType(Module1) : SetCursorPosition(X, Y) : Write(S) : End SyncLock
  End Sub

  Sub Log(ByVal X As String)
    Using F As New StreamWriter(My.Application.Info.AssemblyName & ".log", True) : F.WriteLine(Now.ToString("yyyy-MM-dd HH:mm:ss") & vbTab & X) : End Using
  End Sub

  Sub Log(ByVal X As String, Optional N As String = "PegasusPnL")
    Using F As New StreamWriter(N & ".log", True) : F.WriteLine(Now.ToString("yyyy-MM-dd HH:mm:ss") & vbTab & X) : End Using
  End Sub

  Sub LogFile(ByVal X As String, ByVal Y As String)
    Using F As New StreamWriter(Y & ".log", True) : F.WriteLine(Now.ToString("yyyy-MM-dd HH:mm:ss") & vbTab & X) : End Using
  End Sub

  Sub wCSV(ByVal X As String, ByVal Y As String)
    Using F As New StreamWriter(Y & Now.Date.ToString("yyyy-MM-dd") & ".csv", True) : F.WriteLine(X) : End Using
  End Sub

  '-- Database Related Functions ------------------------------------------------
  Function Conn(Optional SVR As Byte = 0) As SqlConnection
    Try
      Dim X As New SqlConnection(My.Settings("dbConnStr_" & SVR)) : X.Open() : Return X
    Catch : Return Nothing : Log("DB Connection Error") : End Try
  End Function

  Sub execSQL(SQL As String, Optional SVR As Byte = 0, Optional WAIT As Byte = 30)
    Dim CM As New SqlCommand(SQL, Conn(SVR)) : CM.CommandTimeout = WAIT : CM.ExecuteNonQuery() : CM.Connection.Close()
  End Sub

  Function runSQL(SQL As String, Optional SVR As Byte = 0, Optional WAIT As Byte = 30) As Thread
    Dim P() As Object = {SQL, SVR, WAIT}, T As New Thread(AddressOf _runSQL) : T.Start(P) : Return T
  End Function

  Sub _runSQL(P As Object)
    Dim CM As New SqlCommand(P(0), Conn(P(1))) : CM.CommandTimeout = P(2) : CM.ExecuteNonQuery() : CM.Connection.Close()
  End Sub

  Function getRecord(SQL As String, Optional SVR As Byte = 0) As SqlDataReader
    Return New SqlCommand(SQL, Conn(SVR)).ExecuteReader(CommandBehavior.CloseConnection)
  End Function

  Function getResult(SQL As String, Optional SVR As Byte = 0, Optional VL As Object = "") As Object
    Dim CM As New SqlCommand(SQL, Conn(SVR)), DT As Object = CM.ExecuteScalar()
    CM.Connection.Close() : Return If(IsDBNull(DT), VL, DT)
  End Function

  Function makeDataSet(SQL As String, Optional SVR As Byte = 0) As DataSet
    Try
      Dim CN As SqlConnection = Conn(SVR)
      Dim DA As New SqlDataAdapter() : DA.SelectCommand = New SqlCommand(SQL, CN)
      Dim DS As New DataSet : DA.Fill(DS) : DA.Dispose() : CN.Close() : Return DS
    Catch : Log("DataSet" & vbTab & Err.Description) : Return Nothing : End Try
  End Function

  Function postWeb(URL As String, DATA As String) As String
    Try : Dim W As New WebClient() : W.Headers("Content-Type") = "application/x-www-form-urlencoded"
      Return W.UploadString(URL, DATA) : Catch : Return Err.Description : End Try
  End Function

  Function getWeb(URL As String) As String
    Try : Dim RQ As HttpWebRequest = WebRequest.Create(URL) : RQ.AutomaticDecompression = True
      Dim RP As HttpWebResponse = RQ.GetResponse(), DS As Stream = RP.GetResponseStream(), SR As New StreamReader(DS), RS As String = SR.ReadToEnd()
      SR.Close() : DS.Close() : RP.Close() : Return RS : Catch : Return "SVR_ERR" : End Try
  End Function

  Function sNS(ByVal X As Object) As String
    If IsNothing(X) Then Return "NULL" Else Return "'" & CStr(X).ToUpper.Replace("'", "''") & "'"
  End Function

  Function sTime(ByVal X As Object) As String
    If IsNothing(X) OrElse X.ToString = "" Then Return "" Else Return CDate(X).ToString("yyyy-MM-dd HH:mm:ss")
  End Function

  Function Flooring(ByVal X As Double) As Double
    Return Math.Floor(X * 10) / 10
  End Function

  Function Batch2Epoch(N As String) As ULong
    Return DateDiff("s", "1970-01-01", Date.UtcNow.ToString("yyyy-01-01")) + Convert.ToInt32(N, 16)
  End Function

  Function ICase(K As Object, ByVal ParamArray C() As Object) As Object
    Dim L As Byte = C.Length, I As Byte = 0
    While I + 1 < L : If K = C(I) Then : Return C(I + 1) : End If : I += 2 : End While
    Return If(L Mod 2 = 1, C(L - 1), Nothing)
  End Function

  Function sNull(X As Object, Y As Object) As Object
    If IsDBNull(X) Then Return Y Else Return X
  End Function

#End Region

End Module
