﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web.Script.Serialization
Imports System.Xml


Public Class WebAccess
  Private RQ As HttpWebRequest, CK As New CookieContainer,
          USR As String = "", PWD As String


  Sub New(Optional mUSR As String = "", Optional mPWD As String = "")
    If mUSR <> "" Then
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
      USR = mUSR : PWD = mPWD
    End If
  End Sub


  Public Function Load(URL As String, Optional DTA As String = "", Optional JSN As String = "") As String
    System.Net.ServicePointManager.SecurityProtocol = DirectCast(3072, System.Net.SecurityProtocolType) 'TLS 1.2
    RQ = WebRequest.Create(URL) : RQ.Method = If(DTA = "", "GET", "POST")
    If USR <> "" Then RQ.Credentials = New NetworkCredential(USR, PWD)
    RQ.Accept = "*/*" ': RQ.AutomaticDecompression = True : RQ.CookieContainer = CK : RQ.AllowAutoRedirect = False
    If JSN <> "" Then RQ.ContentType = "application/json"
    RQ.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36"
    'RQ.MaximumResponseHeadersLength = 999999
    'RQ.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36"
    If JSN = "aJSON" Then
      RQ.ContentType = "application/json"
      RQ.Headers.Add("Origin", "https://www.racenet.com.au")
      RQ.Referer = "https://www.racenet.com.au/"
      'RQ.Credentials = CredentialCache.DefaultCredentials
    Else : RQ.ContentType = "application/x-www-form-urlencoded" : End If
    If DTA <> "" Then
      Dim BD As Byte() = Encoding.Default.GetBytes(DTA) : RQ.GetRequestStream().Write(BD, 0, BD.Length)
    End If
    Dim RP As HttpWebResponse = RQ.GetResponse(), SR As New StreamReader(RP.GetResponseStream())
    Dim WR As String = SR.ReadToEnd() : SR.Close() : RP.Close() : Return WR
  End Function

  Public Function WCLoad(URL As String, Optional DTA As String = "") As String
    Dim RW As WebClient = New WebClient()
    RW.Headers.Add("Content-Type", "application/json")
    Dim BD As Byte() = Encoding.UTF8.GetBytes(DTA)
    Dim RP As Byte() = RW.UploadData(URL, "POST", BD)
    Dim WR As String = Encoding.UTF8.GetString(RP)
  End Function

  Public Function BEZLoad(URL As String, Optional DTA As String = "", Optional JSN As String = "") As String
    RQ = WebRequest.Create(URL) : RQ.Method = If(DTA = "", "GET", "POST") : RQ.Host = "beteasy.com.au"
    If USR <> "" Then RQ.Credentials = New NetworkCredential(USR, PWD)
    RQ.Accept = "gzip" : RQ.AutomaticDecompression = True : RQ.CookieContainer = CK : RQ.AllowAutoRedirect = False
    If JSN <> "" Then RQ.ContentType = "application/json"
    RQ.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36"
    If DTA <> "" Then : RQ.ContentType = "application/x-www-form-urlencoded"
      Dim BD As Byte() = Encoding.Default.GetBytes(DTA) : RQ.GetRequestStream().Write(BD, 0, BD.Length)
    End If
    Dim RP As HttpWebResponse = RQ.GetResponse(), SR As New StreamReader(RP.GetResponseStream())
    Dim WR As String = SR.ReadToEnd() : SR.Close() : RP.Close() : Return WR
  End Function

  Public Function Post(URL As String, HST As String) As String
    RQ = WebRequest.Create(URL) : RQ.Method = "POST"
    If USR <> "" Then RQ.Credentials = New NetworkCredential(USR, PWD)
    RQ.Accept = "gzip, deflate" : RQ.AutomaticDecompression = True : RQ.CookieContainer = CK
    RQ.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)"
    'RQ.ContentType = "application/x-www-form-urlencoded"
    RQ.ContentLength = 0
    RQ.Host = HST : RQ.Referer = "http://" & HST & "/" : RQ.Headers.Add("Origin", "http://" & HST)
    Dim RP As HttpWebResponse = RQ.GetResponse(), SR As New StreamReader(RP.GetResponseStream())
    Dim WR As String = SR.ReadToEnd() : SR.Close() : RP.Close() : Return WR
  End Function


  Public Function JSON(URL As String, Optional DTA As String = "") As Object
    Try : Return New JavaScriptSerializer().Deserialize(Of Object)(Load(URL, DTA, "JSON")) : Catch : Return Nothing : End Try
  End Function

  Public Function aJSON(URL As String, Optional DTA As Object = Nothing) As Object
    'Try
    Return New JavaScriptSerializer().Deserialize(Of Object)(Load(URL, DTA, "aJSON"))
    'Catch : Return Nothing : End Try
  End Function

  Public Function JSONit(S As String) As Object
    Try : Return New JavaScriptSerializer().Deserialize(Of Object)(S) : Catch : Return Nothing : End Try
  End Function


  Public Sub Cookies(URL As String)
    Dim S As New StringBuilder : S.Append(URL & " Cookies:" & vbLf & "".PadRight(67, "-") & vbLf)
    For Each C As Cookie In CK.GetCookies(New Uri(URL)) : S.Append(Left(C.Name, 25).PadRight(26) & "= " & Left(C.Value, 39) & vbLf) : Next
    Ink(S.ToString & "".PadRight(67, "-"))
  End Sub

End Class
